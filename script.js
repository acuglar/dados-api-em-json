const fetch = require('node-fetch')
const fs = require('fs')

function randomNum() {
    let random = Math.floor(Math.random() * 100) + 1
    fetch(`http://numbersapi.com/${random}`)
        .then(res => res.text())
        .then(data => {
            fs.writeFile("dados.json", data, function (err) {
                //Caro ocorra algum erro
                if (err) {
                    return console.log('erro')
                }
                //Caso sucesso
                console.log('Arquivo Criado');
            })
        })
}

randomNum()

/* 
No exemplo abaixo, informamos o local que será criado o arquivo
toda a informação que esse arquivo conterá, e por ultimo temos nossa função callback
fs.writeFile("./files/example.txt", 'Um breve texto aqui!', function (err) {
    //Caro ocorra algum erro
    if (err) {
        return console.log('erro')
    }
    //Caso não tenha erro, retornaremos a mensagem de sucesso
    console.log('Arquivo Criado');
});
*/